package com.graysail.opencms.module.logevents;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.opencms.configuration.CmsConfigurationManager;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsEvent;
import org.opencms.main.CmsLog;
import org.opencms.main.I_CmsEventListener;
import org.opencms.main.OpenCms;
import org.opencms.module.A_CmsModuleAction;
import org.opencms.module.CmsModule;

/**
 * This is a very simple OpenCms module action that registers for events
 * and logs them (and any associated data).
 * 
 * This class will need to be registered as the Action Class of an OpenCms module
 * in order to function.
 * 
 * @author Mitch Martin
 */
public class LogRegisteredEventsModuleAction extends A_CmsModuleAction {

	/** Log for this Module Action */
	private static Log LOG = CmsLog.getLog( LogRegisteredEventsModuleAction.class );
    
	@Override
	public void initialize(CmsObject adminCms, CmsConfigurationManager configurationManager, CmsModule module) {
		
		// Initialization
		super.initialize(adminCms, configurationManager, module);
		
		// Depending on what our module will do, we may want to keep a local reference to
		//    CmsObject, CmsConfigurationManager, or CmsModule.
		
		//Register for Events
		this.registerEventListeners();
	}
	
	@Override
	public void cmsEvent(CmsEvent event) {
		super.cmsEvent(event);
		
		//This is our entry point for handling any and all CmsEvents that we
		//  registered for in #registerEventListeners.
		
		//If behavior is specific to a type of event, we should check for the
		//  event types here
		
		this.logEventDetails(event);
	}
	
	/**
	 * Registers this action to listen for events.
	 * 
	 * @see I_CmsEventListener Event ID Constants for available events
	 */
	private void registerEventListeners(){
		
		/** Array of event types for which this action should listen. */
		int[] eventTypes = {
			I_CmsEventListener.EVENT_USER_MODIFIED
		};
		
		//Tell OpenCms that we want to be notified when
		//  these events are fired
		OpenCms.addCmsEventListener(this, eventTypes);
	}
	
	/**
	 * Logs event details to the log.
	 * 
	 * @see CmsEvent#getData()
	 * @param event
	 *   The {@link CmsEvent} to log
	 */
	private void logEventDetails(CmsEvent event){
		
		// Log that we are observing this event, and the type of this event
		LOG.info( String.format("Observing Event of type %d.", event.getType() ));
		
		//If the event has associated data, log that too
		Map<String, Object> data = event.getData();
		
		if( data != null && !data.isEmpty() ){
			LOG.info("Event Data:");
			for( String key : data.keySet() ){
				String value = (String) data.get(key);
				
				LOG.info( String.format("  %s = %s", key, value));
			}
		}
	}
	
}
