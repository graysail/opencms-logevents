# Sample OpenCms Module Action
This module is a very simple implementation of an OpenCms extension. While active, it will write information about any events that OpenCms fires, so long as it has registered to be notified of those events.

Below is some sample output. We can see the details of events related to removing a user (named 'test').

```
Observing Event of type 29. Event Data:
  groupId = 6dfffebb-0985-3cbd-8d53-a5df8681a9f3
  groupName = Users
  userId = 8c175314-da13-11e4-baa5-0026b916e341
  userAction = removeUserFromGroup
  userName = test
Observing Event of type 29. Event Data:
  groupId = 83a2baf5-6636-322c-9665-41b89d837a83
  groupName = RoleElementAuthor
  userId = 8c175314-da13-11e4-baa5-0026b916e341
  userAction = removeUserFromGroup
  userName = test
Observing Event of type 29. Event Data:
  userId = 8c175314-da13-11e4-baa5-0026b916e341
  userAction = deleteUser
  userName = test
```

# Installation
To see it in action, simply install the `com.graysail.opencms.module.logevents_1.0.zip` module and restart the servlet container. Note that you may also need to adjust your logger configuration; this module will log INFO-level messages with the `com.graysail.opencms.module.logevents.LogRegisteredEventsModuleAction` logger.

The module can be downloaded from the Bitbucket repository's [Downloads page](downloads). Alternatively, you can build the 1.0 version from this repository with Gradle using the `build dist` Gradle tasks. The resulting module zip will be created in `build/distributions/`.

#OpenCms Module Actions
The key concept of this sample module is the Module Action, also referred to as Action Class. A working module action extends `org.opencms.module.A_CmsModuleAction` and is registered as a module's Action Class. OpenCms notifies Action Classes to allow them to add functionality to OpenCms. Most of the time, this functionality revolves around the content lifecycle, such as when creating, modifying, publishing, or deleting content. See the documentation (or source) for `org.opencms.module.A_CmsModuleAction` for more information.

## Registering for Events
In order for an Action Class to be called when an event is fired, it must be registered for that event's type. Registering for events is very easy, and should be done on the action class' initialization. This is done by calling `org.opencms.main.OpenCms#addCmsEventListener`. In the example below, the action class is registering for all "User Modified" and "Group Modified" events.

```
	@Override
	public void initialize(CmsObject adminCms, CmsConfigurationManager configurationManager, CmsModule module) {
		super.initialize(adminCms, configurationManager, module);
		
		int[] eventTypes = {
			I_CmsEventListener.EVENT_USER_MODIFIED,
			I_CmsEventListener.EVENT_GROUP_MODIFIED
		};
		
		OpenCms.addCmsEventListener(this, eventTypes);
	}
```

## Acting on Events
Once registered, OpenCms will pass a fired `CmsEvent` to all listeners (such as our Action Class) that are registered for that event's type. Any actions to be performed must be implemented in the Action Class' `cmsEvent` function.

In the example below, the implementation is just logging details of any event:
```
	@Override
	public void cmsEvent(CmsEvent event) {
		super.cmsEvent(event);
		
		LOG.info( String.format("Observing Event of type %d.", event.getType() ));
		
		Map<String, Object> data = event.getData();
		
		if( data != null && !data.isEmpty() ){
			LOG.info("Event Data:");
			for( String key : data.keySet() ){
				String value = (String) data.get(key);
				
				LOG.info( String.format("  %s = %s", key, value));
			}
		}
	}
```